

public with sharing class FunctionTest{

    @AuraEnabled
    public static String getExportXLSX(){
        Functions.Function function = Functions.Function.get('SalesforceFunctionUltima.generateimage');
        String result = function.invoke('{}').getResponse();
        System.debug('result = ');
        return ResponseWrapper.parse(result).xlsxBase64 ;
    }
}