/**
 * Created by William on 01/11/2022.
 */

public with sharing class ResponseWrapper {
    public String xlsxBase64;

    public static ResponseWrapper parse(String json){
        return (ResponseWrapper) System.JSON.deserialize(json, ResponseWrapper.class);
    }
}