package com.example;

import com.salesforce.functions.jvm.sdk.Context;
import com.salesforce.functions.jvm.sdk.InvocationEvent;
import com.salesforce.functions.jvm.sdk.SalesforceFunction;
import com.salesforce.functions.jvm.sdk.data.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Describe ExportaccountFunction here.
 */
public class ExportaccountFunction implements SalesforceFunction<ExportaccountInput, ExportaccountOutput> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportaccountFunction.class);

    @Override
    public ExportaccountOutput apply(InvocationEvent<ExportaccountInput> event, Context context)
            throws Exception {

        List<Record> records =
                context.getOrg().get().getDataApi().query("SELECT Id, Name FROM Account").getRecords();

        LOGGER.info("Function successfully queried {} account records!", records.size());

        List<Account> accounts = new ArrayList<>();
        for (Record record : records) {
            String id = record.getStringField("Id").get();
            String name = record.getStringField("Name").get();
            accounts.add(new Account(id, name));
        }


        return new ExportaccountOutput(CreateWorksheet.createWorksheetFromAccount(accounts));
    }
}
