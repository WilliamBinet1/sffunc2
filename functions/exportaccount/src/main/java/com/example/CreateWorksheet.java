package com.example;


import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.List;

public class CreateWorksheet {

    public static String createWorksheetFromAccount(List<Account> accounts) {
        Workbook workbook = new Workbook();
        Worksheet worksheet = workbook.getWorksheets().add("MES COMPTES");
        worksheet.getCells().insertRows(0, accounts.size());

        for (int i = 0; i < accounts.size(); i++) {
            worksheet.getCells().get(i, 0).setValue(accounts.get(i).getId());
            worksheet.getCells().get(i, 1).setValue(accounts.get(i).getName());
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            workbook.save(stream, SaveFormat.XLSX);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return  new String(Base64.getEncoder().encode(stream.toByteArray()));
    }

}
